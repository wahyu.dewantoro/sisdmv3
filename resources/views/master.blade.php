

@extends('adminlte::page')

@section('title', 'SISDM v3')
@section('content_header')
    <h1>@yield('judul_halaman')</h1>
    <ol class="breadcrumb">
    	<li>@yield('kanan_atas')</li>
      </ol>    
@stop

@section('content')
    @yield('konten')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop


 