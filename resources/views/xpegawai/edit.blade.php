



@extends('master')

@section('judul_halaman', 'Edit Pegawai')



@section('konten')

<form action="/pegawai/update/{{$pegawai->pegawai_id}}" method="post">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
	@include('pegawai._form')

</form>



@endsection