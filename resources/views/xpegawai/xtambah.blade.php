<!-- Menghubungkan dengan view template master -->
@extends('master')

<!-- isi bagian judul halaman -->
<!-- cara penulisan isi section yang pendek -->
@section('judul_halaman', 'Data pegawai')


<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('konten')

	
	<a href="/pegawai"> Kembali</a>
	<form action="/pegawai/store" method="post">
		<div class="form-group">
			Nama <input type="text" name="nama" required="required" class="form-control"> 
		</div>
		<div class="form-group">
			Jabatan <input type="text" name="jabatan" required="required" class="form-control"> 
		</div>
		<div class="form-group">
			Umur <input type="number" name="umur" required="required" class="form-control"> 
		</div>
		<div class="form-group">
			Alamat <textarea class="form-control" name="alamat" required="required"></textarea > 
		</div>

		{{ csrf_field() }}
		
		<input type="submit" value="Simpan Data">
	</form>

@endsection

  