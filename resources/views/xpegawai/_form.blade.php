<div class="form-group">
			Nama <input type="text" name="nama" id="nama" required="required" class="form-control" value="{{ $pegawai->pegawai_nama}}"> 
			@if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                            @endif
		</div>
		<div class="form-group">
			Jabatan <input type="text" name="jabatan" id="jabatan" required="required" value="{{ $pegawai->pegawai_jabatan}}" class="form-control"> 
			@if($errors->has('jabatan'))
                                <div class="text-danger">
                                    {{ $errors->first('jabatan')}}
                                </div>
                            @endif
		</div>
		<div class="form-group">
			Umur <input type="number" name="umur" id="umur" required="required" value="{{ $pegawai->pegawai_umur}}" class="form-control"> 
			@if($errors->has('umur'))
                                <div class="text-danger">
                                    {{ $errors->first('umur')}}
                                </div>
                            @endif
		</div>
		<div class="form-group">
			Alamat <textarea class="form-control" id="umur" name="alamat" required="required">{{ $pegawai->pegawai_alamat}}</textarea > 
			@if($errors->has('alamat'))
	            <div class="text-danger">
	                {{ $errors->first('alamat')}}
	            </div>
	        @endif
		</div>
		<input type="submit" value="Simpan Data">




