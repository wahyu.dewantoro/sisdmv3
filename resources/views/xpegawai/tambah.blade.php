

@extends('master')

@section('judul_halaman', 'Tambah Pegawai')



@section('konten')

<form action="/pegawai/store" method="post">
	{{ csrf_field() }}
	@include('pegawai._form')

</form>
@endsection
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop
