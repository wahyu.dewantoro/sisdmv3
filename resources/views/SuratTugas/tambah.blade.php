
@extends('master')
@section('judul_halaman', 'Tambah Pegawai')
@section('kanan_atas')
<a href="/st" > <i class="fa  fa-angle-double-left"></i> kembali</a>
@endsection
@section('konten')
		<form action="/st/store" method="post">
			{{ csrf_field() }}
			@include('SuratTugas._form')

		</form>
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
@stop
