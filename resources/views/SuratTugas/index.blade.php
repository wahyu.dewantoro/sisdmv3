
@extends('master')
@section('judul_halaman', 'Tugas Kedinasan')
@section('kanan_atas')
<a href="/st/tambah" class="btn btn-xs btn-success"> <i class="fa fa-plus"></i> Tambah</a>
@endsection

@section('konten')

 <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Tugas</h3>

              <div class="box-tools">

              	<form action="/st" method="GET">
					<!-- <input type="text" name="cari" placeholder="Cari Pegawai .." value="{{ $cari }}">
					<input type="submit" value="CARI"> -->
					<div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <input type="text" name="cari" value="{{ $cari }}" class="form-control pull-right" placeholder="cari data ...">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>


                  </div>
                </div>
				</form>

                
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            	
              <table class="table table-hover table-bordered">
                <thead>
                	<tr>
						<th>KD Unit</th>
						<th>No Surat</th>
						<th>Tanggal Surat</th>
						<th>Perihal</th>
						<th>Type</th>
						<th>Mulai</th>
						<th>Selesai</th>
                	</tr>
                </thead>
                <tbody>
                	@foreach($data as $p)
                	<tr>
						<td>{{ $p->KdUnitOrg }}</td>
						<td>{{ $p->NoSurat }}</td>
						<td>{{ $p->TglSurat }}</td>
						<td>{{ $p->Perihal }}</td>
						<td>{{ $p->Type }}</td>
						<td>{{ $p->TanggalMulai }}</td>
						<td>{{ $p->TanggalSelesai }}</td>
                	</tr>
                		@endforeach
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
            	{{ $data->links() }}
          </div>
          <!-- /.box -->
        </div>
      </div>

@endsection

 