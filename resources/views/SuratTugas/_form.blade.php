<div class="box">
	<div class="box-body">
		<div class="row">
			<div class="col-md-6">
				<!-- <div class="form-group">
					<label>kd_unit_org  </label>
					<input type="text" name="kd_unit_org" id="kd_unit_org" class="form-control" required >
					
				</div> -->
				<strong>Surat Tugas :</strong>
				<div class="form-group">
					<label>Nomor Surat <sup>*</sup></label>
					<input type="text" name="no_surat" id="no_surat" class="form-control" required >
					@if($errors->has('no_surat'))
						<span class="help-block">{{ $errors->first('no_surat')}}</span>
					@endif
				</div>
				<div class="form-group">
					<label>Tanggal <sup>*</sup></label>
					<input type="text" name="tgl_surat" id="tgl_surat" class="form-control datepicker" required >
					@if($errors->has('tgl_surat'))
						<span class="help-block">{{ $errors->first('tgl_surat')}}</span>
					@endif
				</div>
				<div class="form-group">
					<label>Perihal <sup>*</sup></label>
					<input type="text" name="perihal" id="perihal" class="form-control" required >
					@if($errors->has('perihal'))
						<span class="help-block">{{ $errors->first('perihal')}}</span>
					@endif
				</div>
				<div class="form-group">
					<label>Tanggal Mulai <sup>*</sup></label>
					<input type="text" name="tgl_mulai" id="tgl_mulai" class="form-control datepicker" required >
					@if($errors->has('tgl_mulai'))
						<span class="help-block">{{ $errors->first('tgl_mulai')}}</span>
					@endif
				</div>
				<div class="form-group">
					<label>Tanggal Selesai <sup>*</sup></label>
					<input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control datepicker" required >
					@if($errors->has('tgl_selesai'))
						<span class="help-block">{{ $errors->first('tgl_selesai')}}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<strong>Pegawai :</strong>
				<div class="input-group">
                <select class="cari form-control" style="width:100%s;" id="cari" name="cari"></select>
                <!-- <div class="input-group-btn">
                  <button  class="btn btn-success"  id="tambahdiag"><i class="fa fa-plus"></i> Tambah</button>         
                </div> -->
             
              </div>

                <table class="table table-striped">
                	<thead>
                		<tr>
                			<th>NIP</th>
                			<th>Nama</th>
                			<th></th>
                		</tr>
                	</thead>
                	<tbody id="kontentdiag"></tbody>
                </table>
			</div>
		</div>
	</div>  
	<div class="box-footer">
		<div class="pull-right">
			<button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
		</div>
	</div>          
</div>
@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
 <script type="text/javascript">
  $('.cari').select2({
    placeholder: 'Cari...',
    ajax: {
      url: '/st/pegawai',
      dataType: 'json',
      delay: 250,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.nip+' - '+item.nm_peg,
              id: item.nip
            }
          })
        };
      },
      cache: true
    }
  });

   $('.datepicker').datepicker({
      autoclose: true
    })

   // $("#tambahdiag").change(function(e){
   	$('#cari').on('select2:select', function (e) {
                              e.preventDefault();
                             $('#kontentdiag').append(
                                      '<tr class="barisdiag">'                                      
                                         +'<input type="hidden" value="'+ $('#cari').val() +'" class="form-control" name="nip[]">'
                                     + '<td>'+ $('#cari').val() +'</td>'
                                     + '<td>'+ $('#cari').find("option:selected").text()+'</td>'
                                     + '<td align=\"center\"><a href="#" class="remove_project_file"  ><i class="btn btn-xs btn-danger fa fa-trash"></i></a></td></tr>'
                                );
                            
                            	$('#cari').val(null).trigger('change');

                      });

 $('#kontentdiag').on('click', '.remove_project_file', function(e) {
                          e.preventDefault();
                          
                          $(this).parents(".barisdiag").remove();
                      });

</script>
@stop