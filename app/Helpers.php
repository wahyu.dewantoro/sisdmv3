<?php

function dateConverter($date)
{
    if ($date != null) {
        $formated = str_replace('/', '-', $date);
        $dateFormated = Carbon\Carbon::parse($formated);
        return $dateFormated->format('Y-m-d');
    } else {
        return null;
    }
}