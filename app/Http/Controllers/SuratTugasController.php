<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use View;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Surattugas;
use App\Surattugasdetail;

class SuratTugasController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
		$cari = $request->cari;
    	$st = SuratTugas::filtered()->paginate(10);
    	return view('SuratTugas.index',['data' => $st,'cari'=>$cari]);
 
    }

    public function tambah(){
    		$data=array(
    		'st' => new Surattugas(),
    	);
    	return view('SuratTugas.tambah',$data);
    }

    public function daftarPegawai(Request $request){
        if ($request->has('q')) {
            $cari = $request->q;
            $data = DB::table('SISDM_DATA_CURRENT')->select('nip', 'nm_peg')->where('nip', 'LIKE', '%'.$cari.'%')->orWhere('nm_peg', 'LIKE', '%'.$cari.'%')->get();
            return response()->json($data);
        }
    }

     public function store(Request $request)
    {
     
        $this->validate($request,[
            'no_surat'=>'required',
            'tgl_surat'=>'required',
            'perihal'=>'required',
            'tgl_mulai'=>'required',
            'tgl_selesai'=>'required',
        ]);

        // echo Tanggal::dateConverter();
     

    /*$tgl=$request->tgl_surat;
    $dt = new  \Carbon\Carbon($tgl);
    setlocale(LC_TIME, 'IND');
    echo $dt->formatLocalized('%Y-%m-%d');*/

    // echo $dt->formatLocalized('%A, %e %B %Y'); 
    // Senin, 3 September 2018


        // echo dateConverter($request->tgl_surat);

      /*  $date=$request->tgl_surat;
         if ($date != null) {
            $formated = str_replace('/', '-', $date);
            $dateFormated = Carbon\Carbon::parse($formated);
            echo $dateFormated->format('Y-m-d');
        } else {
            echo null;
        }*/

        // dd($request);


      /*  Pegawai::create([
            'pegawai_nama' => $request->nama,
            'pegawai_jabatan' => $request->jabatan,
            'pegawai_umur' => $request->umur,
            'pegawai_alamat' => $request->alamat
        ]);
        return redirect('/pegawai');*/
    }
}


    
