<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use View;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\Pegawai;

class PegawaiController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
		$cari = $request->cari;
    	$pegawai = Pegawai::filtered()->paginate(5);
   
    	return view('pegawai.index',['pegawai' => $pegawai,'cari'=>$cari]);
 
    }

    public function tambah(){

    	$data=array(
    		'pegawai' => new Pegawai(),
    	);

    	// dd($data['pegawai']->pegawai_id);
    	return view('pegawai.tambah',$data);
    }

    public function store(Request $request)
	{
	 
		$this->validate($request,[
			'nama'    =>'required',
			'jabatan' =>'required',
			'umur'    =>'required',
			'alamat'  =>'required',
    	]);
 
        Pegawai::create([
			'pegawai_nama' => $request->nama,
			'pegawai_jabatan' => $request->jabatan,
			'pegawai_umur' => $request->umur,
			'pegawai_alamat' => $request->alamat
    	]);
		return redirect('/pegawai');
	}

	public function edit($id){	
		$pegawai = Pegawai::find($id);
		return view('pegawai.edit',['pegawai' => $pegawai]);
	}
	
	// update data pegawai
	public function update($id,Request $request)
	{

		$this->validate($request,[
			'nama'    =>'required',
			'jabatan' =>'required',
			'umur'    =>'required',
			'alamat'  =>'required',
    	]);

			$pegawai = Pegawai::find($id);
			$pegawai->pegawai_nama = $request->nama;
			$pegawai->pegawai_jabatan = $request->jabatan;
			$pegawai->pegawai_umur = $request->umur;
			$pegawai->pegawai_alamat = $request->alamat;
			$pegawai->save();

		return redirect('/pegawai');
	}

	public function hapus($id)
	{
		$pegawai = Pegawai::find($id);
    	$pegawai->delete();
			
		return redirect('/pegawai');
	}
}
