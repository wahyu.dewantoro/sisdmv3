<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    //
    protected $table = "pegawai";
    protected $primaryKey = 'pegawai_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $guarded = [];
    public $sortable = ['pegawai_nama'];

    public function scopeFiltered($query)
    {
        $query->when(request('cari'), function ($query) {
            $query->where(function ($query) {
                $param = '%' . request('cari') . '%';
                $query->where('pegawai_nama', 'like', $param)
				->orWhere('pegawai_jabatan','like',$param)
				->orWhere('pegawai_umur','like',$param)
				->orWhere('pegawai_alamat','like',$param);

            });
        });

       
    }
}
