<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surattugasdetail extends Model
{
    //
      protected $table = "SuratTugasDetail";
     /*protected $primaryKey = 'id';
    public $incrementing = true;*/
    public $timestamps = true;

    protected $guarded = [];
}
