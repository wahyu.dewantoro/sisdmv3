<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Surattugas extends Model
{
    //
     protected $table = "SuratTugasHeader";
     protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = true;

    protected $guarded = [];

     public function scopeFiltered($query)
    {
        $query->when(request('cari'), function ($query) {
            $query->where(function ($query) {
                $param = '%' . request('cari') . '%';
                $query->where('KdUnitOrg','like',$param)
				->orWhere('NoSurat','like',$param)
				->orWhere('TglSurat','like',$param)
				->orWhere('Perihal','like',$param)
				->orWhere('Type','like',$param)
				->orWhere('TanggalMulai','like',$param)
				->orWhere('TanggalSelesai','like',$param);

            });
        });

       
    }
}
