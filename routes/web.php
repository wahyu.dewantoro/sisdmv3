<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('admin/dashboard');
});

	;
*/

Route::get('/', 'HomeController@index')->name('home');



/*// route blog
Route::get('/blog', 'BlogController@home');
Route::get('/blog/tentang', 'BlogController@tentang');
Route::get('/blog/kontak', 'BlogController@kontak');
*/

/*// route pegawai
Route::get('/pegawai','PegawaiController@index');
Route::get('/pegawai/tambah','PegawaiController@tambah');
Route::post('/pegawai/store','PegawaiController@store');
Route::get('/pegawai/edit/{id}','PegawaiController@edit');
// Route::post('/pegawai/update','PegawaiController@update');
Route::put('/pegawai/update/{id}', 'PegawaiController@update');
Route::get('/pegawai/hapus/{id}','PegawaiController@hapus');*/

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');;


 // route surattugas
Route::get('/st','SuratTugasController@index');
Route::get('/st/tambah','SuratTugasController@tambah');
Route::post('/st/store','SuratTugasController@store');
Route::get('/st/edit/{id}','SuratTugasController@edit');
Route::put('/st/update/{id}', 'SuratTugasController@update');
Route::get('/st/hapus/{id}','SuratTugasController@hapus');
Route::get('/st/pegawai','SuratTugasController@daftarPegawai');



